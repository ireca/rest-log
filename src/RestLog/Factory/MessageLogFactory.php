<?php
namespace RestLog\Factory;

use RestLog\Service\Transport\Interfaces\MessageLogInterface;
use RestLog\Service\Transport\MessageLogService;

class MessageLogFactory extends BaseFactory
{
    /**
     * @param $config
     * @return MessageLogInterface
     * @throws \Doctrine\ORM\ORMException
     */
    public static function createByConfig($config)
    {
        return new MessageLogService(
            self::getEntityManagerService($config['restLog']),
            $config['restLog']['isActive']
        );
    }

}