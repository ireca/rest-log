<?php
namespace RestLog\Factory;

use RestLog\Service\Transport\Interfaces\RestLogInterface;
use RestLog\Service\Transport\AppLogService;

class AppLogFactory extends BaseFactory
{
    /**
     * @param $config
     * @param $application
     * @return RestLogInterface
     * @throws \Doctrine\ORM\ORMException
     */
    public static function createByConfig($config, $application)
    {
        $service = new AppLogService(
            self::getEntityManagerService($config['restLog']),
            $config['restLog']['isActive']
        );
        $service->setApplication($application);

        return $service;
    }

}