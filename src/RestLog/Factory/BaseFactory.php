<?php
namespace RestLog\Factory;

use RestLog\Service\EntityManagerService;

class BaseFactory
{
    /**
     * @return \Doctrine\ORM\EntityManager|null
     * @throws \Doctrine\ORM\ORMException
     */
    protected static function getEntityManagerService(array $config)
    {
        $service = new EntityManagerService($config['databaseConfig']);

        return $service->getEm();
    }

}