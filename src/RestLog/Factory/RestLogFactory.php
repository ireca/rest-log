<?php
namespace RestLog\Factory;

use RestLog\Service\Transport\Interfaces\RestLogInterface;
use RestLog\Service\Transport\RestLogService;

class RestLogFactory extends BaseFactory
{
    /**
     * @param $config
     * @return RestLogInterface
     * @throws \Doctrine\ORM\ORMException
     */
    public static function createByConfig($config)
    {
        return new RestLogService(
            self::getEntityManagerService($config['restLog']),
            $config['restLog']['isActive']
        );
    }

}