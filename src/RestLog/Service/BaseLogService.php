<?php
namespace RestLog\Service;

use Doctrine\ORM\EntityManager;
use RestLog\Service\Transport\Interfaces\BaseInterface;

abstract class BaseLogService implements BaseInterface
{
    protected $companyId = null;

    protected $sourceId = null;

    protected $userId = null;

    protected $jsonData = null;

    private $iteration = 0;

    /**
     * @var EntityManager
     */
    private $em = null;

    private $isActive = true;

    /**
     * BaseLogService constructor.
     *
     * @param ConfigService $configService
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em, $isActive)
    {
        $this->em = $em;
        $this->isActive = $isActive;
        $this->iteration = time();
    }

    /**
     * @param $companyId
     * @param null $userId
     * @param null $sourceId
     * @return $this
     */
    public function init($companyId, $userId = null, $sourceId = null)
    {
        $this->companyId = $companyId;
        $this->sourceId = $sourceId;
        $this->userId = $userId;

        return $this;
    }

    /**
     * @param $iteration
     *
     * @return $this
     */
    public function setIteration($iteration)
    {
        $this->iteration = $iteration;

        return $this;
    }

    /**
     * @return int
     */
    public function getIteration()
    {
        return $this->iteration;
    }

    /**
     * @return $this
     */
    protected function setJsonData($data)
    {
        $this->jsonData = null;
        if (! empty($data)) {
            $this->jsonData = ($this->isJson($data)) ? $data : json_encode($data);
        }

        return $this;
    }

    /**
     * @param $data
     * @return bool
     */
    protected function isJson($data)
    {
        if (! is_string($data)) {

            return false;
        }

        json_decode($data);

        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * @return EntityManager
     */
    public function getEm()
    {
        return $this->em;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->isActive;
    }

}