<?php
namespace RestLog\Service\Type;

class LevelType extends BaseType
{
    const ERROR = -1;

    const NOTICE = 1;

    const INFO = 2;

    const DEBUG = 3;

    const CRITICAL = 4;
}