<?php
namespace RestLog\Service;

use Doctrine\ORM\OptimisticLockException;
use RestLog\Model\Log;
use RestLog\Model\LogData;
use RestLog\Model\LogEntity;
use  RestLog\Service\Transport\Interfaces\RestLogInterface;
use RestLog\Service\Type\Command;
use RestLog\Service\Type\CommandType;
use RestLog\Service\Type\LevelType;

abstract class BaseRestLogService extends BaseLogService implements RestLogInterface
{
    protected $application = '';

    protected $sessionId = '';

    protected $count = null;

    private $entity = '';

    /**
     * @param string $command
     * @param string $entity
     * @param string $title
     * @return $this
     * @throws OptimisticLockException
     */
    public function postToLog($command = '', $entity = '', $title = '')
    {
        $log = new Log();
        $log->commandType = CommandType::REQUEST;
        $log->command = ! empty($command) ? $command : Command::CREATE;

        return $this->flush($log, $entity, $title);
    }

    /**
     * @param string $command
     * @param string $entity
     * @param string $title
     * @return $this
     * @throws \Exception
     */
    public function putToLog($command = '', $entity = '', $title = '')
    {
        $log = new Log();
        $log->commandType = CommandType::REQUEST;
        $log->command = ! empty($command) ? $command : Command::UPDATE;

        return $this->flush($log, $entity, $title);
    }

    /**
     * @param string $command
     * @param string $entity
     * @param string $title
     * @return $this
     * @throws \Exception
     */
    public function getToLog($command = '', $entity = '', $title = '')
    {
        $log = new Log();
        $log->commandType = CommandType::REQUEST;
        $log->command = ! empty($command) ? $command : Command::GET;

        return $this->flush($log, $entity, $title);
    }

    /**
     * @param string $command
     * @param string $entity
     * @param string $title
     * @return $this
     * @throws \Exception
     */
    public function deleteToLog($command = '', $entity = '', $title = '')
    {
        $log = new Log();
        $log->commandType = CommandType::REQUEST;
        $log->command = ! empty($command) ? $command : Command::DELETE;

        return $this->flush($log, $entity, $title);
    }

    /**
     * @param $command
     * @param $responseCode
     * @param string $entity
     * @param string $title
     *
     * @return $this
     * @throws OptimisticLockException
     */
    public function responseToLog($command, $responseCode, $entity = '', $title = '')
    {
        $log = new Log();
        $log->commandType = CommandType::RESPONSE;
        $log->command = $command;
        $log->responseCode = $responseCode;

        return $this->flush($log, $entity, $title);
    }

    /**
     * @param $url
     * @param string $entity
     * @param string $title
     * @return $this
     * @throws OptimisticLockException
     */
    public function callbackToLog($url = '', $entity = '', $title = '')
    {
        $log = new Log();
        $log->commandType = CommandType::REQUEST;
        $log->command = Command::CALLBACK;
        $log->url = $url;

        return $this->flush($log, $entity, $title);
    }

    /**
     * @param $responseCode
     * @param string $entity
     * @param string $title
     * @return $this
     * @throws \Exception
     */
    public function callbackResponseToLog($responseCode, $entity = '', $title = '')
    {
        $log = new Log();
        $log->commandType = CommandType::RESPONSE;
        $log->command = Command::CALLBACK;
        $log->responseCode = $responseCode;

        return $this->flush($log, $entity, $title);
    }

    /**
     * @param \Exception $ex
     * @param string $title
     * @param int $level
     * @return $this
     * @throws OptimisticLockException
     */
    public function exceptionToLog(\Exception $ex, $title = '', $level = LevelType::ERROR)
    {
        $log = new Log();
        $log->commandType = CommandType::EXCEPTION;
        $log->level = $level;
        if (! empty($this->jsonData)) {
            $this->jsonData = ! empty($this->jsonData)
                ? 'Content:' . PHP_EOL . $this->jsonData . PHP_EOL . 'Stack trace:' . PHP_EOL . $ex->getTraceAsString()
                : '';
        } else {
            $this->jsonData = $ex->getTraceAsString();
        }

        if (! empty($title)) {
            $title .= ':' . $ex->getMessage();
        } else {
            $title = $ex->getMessage();
        }

        return $this->flush($log, '', $title);
    }

    /**
     * @param \Exception $ex
     * @param string $title
     * @param int $level
     * @return $this
     * @throws OptimisticLockException
     */
    public function exceptionDbToLog(\Exception $ex, $title = '', $level = LevelType::CRITICAL)
    {
        return $this->exceptionToLog($ex, $title, $level);
    }

    /**
     * @param \Exception $ex
     * @param string $title
     * @param int $level
     * @return $this
     * @throws OptimisticLockException
     */
    public function messageToLog($title = '', $level = LevelType::ERROR)
    {
        $log = new Log();
        $log->commandType = CommandType::MESSAGE;
        $log->command = '';
        $log->level = $level;

        return $this->flush($log, '', $title);
    }

    /**
     * @param Log $log
     * @param $entity
     * @param $title
     *
     * @return $this
     * @throws OptimisticLockException
     */
    protected function flush(Log $log, $entity, $title)
    {
        if (! $this->isActive()) {

            return $this;
        }

        $log->companyId = $this->companyId;
        $log->sourceId = $this->sourceId;
        $log->userId = $this->userId;
        $log->application = $this->getApplication();
        $log->entity = ! empty($entity) ? $entity : $this->entity;
        $log->title = $title;
        $log->count = ! is_null($this->getCount()) ? $this->getCount() : count($this->getCodes());
        $log->createdAt = new \DateTime();
        $log->microsecCreatedAt = microtime(true);
        $log->executedAt = new \DateTime();
        $log->sessionId = $this->getSessionId();
        $log->iteration = $this->getIteration();
        $this->getEm()->persist($log);

        if (! empty($this->jsonData)) {
            $logData = new LogData();
            $logData->data = $this->jsonData;
            $logData->setLog($log);
            $this->getEm()->persist($logData);
        }

        if (count($this->getCodes()) > 0) {
            foreach ($this->getCodes() as $code) {
                if (! empty($code)) {
                    $codeProperty = $this->getCodeProperty();

                    $logEntity = new LogEntity();
                    $logEntity->companyId = $this->companyId;
                    $logEntity->sourceId = $this->sourceId;
                    $logEntity->entity = $log->entity;
                    $logEntity->$codeProperty = $code;
                    $logEntity->createdAt = new \DateTime();
                    $logEntity->setLog($log);
                    $this->getEm()->persist($logEntity);
                }
            }
        }

        $this->getEm()->flush();

        return $this;
    }

    /**
     * @param string $application
     *
     * @return BaseRestLogService
     */
    public function setApplication($application)
    {
        $this->application = $application;

        return $this;
    }

    /**
     * @return string
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @return string
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param $count
     *
     * @return $this
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

}