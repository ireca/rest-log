<?php
namespace RestLog\Service\Transport\Interfaces;

use RestLog\Service\Type\LevelType;

interface RestLogInterface extends RestLogStorageInterface
{
    /**
     * @param string $command
     * @param string $entity
     * @param string $title
     * @return mixed
     */
    public function postToLog($command = '', $entity = '', $title = '');

    /**
     * @param string $command
     * @param string $entity
     * @param string $title
     * @return mixed
     */
    public function putToLog($command = '', $entity = '', $title = '');

    /**
     * @param string $command
     * @param string $entity
     * @param string $title
     * @return mixed
     */
    public function getToLog($command = '', $entity = '', $title = '');

    /**
     * @param string $command
     * @param string $entity
     * @param string $title
     * @return mixed
     */
    public function deleteToLog($command = '', $entity = '', $title = '');

    /**
     * @param string $url
     * @param string $entity
     * @param string $title
     * @return mixed
     */
    public function callbackToLog($url = '', $entity = '', $title = '');

    /**
     * @param $responseCode
     * @param string $entity
     * @param string $title
     * @return mixed
     */
    public function callbackResponseToLog($responseCode, $entity = '', $title = '');

    /**
     * @param $command
     * @param $responseCode
     * @param string $entity
     * @param string $title
     * @return mixed
     */
    public function responseToLog($command, $responseCode, $entity = '', $title = '');

    /**
     * @param string $title
     * @param int $level
     * @return mixed
     */
    public function messageToLog($title = '', $level = LevelType::ERROR);

    /**
     * @param \Exception $ex
     * @param int $level
     * @return mixed
     */
    public function exceptionToLog(\Exception $ex, $title = '', $level = LevelType::ERROR);

    /**
     * @param \Exception $ex
     * @param string $title
     * @param int $level
     * @return mixed
     */
    public function exceptionDbToLog(\Exception $ex, $title = '', $level = LevelType::CRITICAL);

}