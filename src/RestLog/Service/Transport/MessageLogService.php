<?php
namespace RestLog\Service\Transport;

use RestLog\Model\Log;
use RestLog\Service\BaseLogService;
use RestLog\Service\Transport\Interfaces\MessageLogInterface;
use RestLog\Service\Type\LevelType;

class MessageLogService extends BaseLogService implements MessageLogInterface
{
    /**
     * @param $message
     * @return $this
     * @throws \Exception
     */
    public function error($message)
    {
        $log = new Log();
        $log->level = LevelType::ERROR;
        $log->title = $message;

        return $this->flush($log);
    }

    /**
     * @param $message
     * @return $this
     * @throws \Exception
     */
    public function info($message)
    {
        $log = new Log();
        $log->level = LevelType::INFO;
        $log->title = $message;

        return $this->flush($log);
    }

    /**
     * @param $message
     * @return $this
     * @throws \Exception
     */
    public function debug($message)
    {
        $log = new Log();
        $log->level = LevelType::DEBUG;
        $log->title = $message;

        return $this->flush($log);
    }

    /**
     * @param $message
     * @return $this
     * @throws \Exception
     */
    public function critical($message)
    {
        $log = new Log();
        $log->level = LevelType::CRITICAL;
        $log->title = $message;

        return $this->flush($log);
    }

    /**
     * @param Log $log
     * @return $this
     * @throws \Exception
     */
    private function flush(Log $log)
    {
        if (! $this->isActive()) {

            return $this;
        }

        $log->companyId = $this->companyId;
        $log->sourceId = $this->sourceId;
        $log->userId = $this->sourceId;
        $log->createdAt = new \DateTime();
        $log->responseCode = null;
        $log->iteration = $this->getIteration();
        $this->getEm()->persist($log);
        $this->getEm()->flush();

        return $this;
    }

}