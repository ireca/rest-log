<?php
namespace RestLog\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

class EntityManagerService
{
    private $databaseConfig = array();

    /**
     * @var EntityManager
     */
    private $em = null;

    /**
     * EntityManagerService constructor.
     *
     * @param array $databaseConfig
     */
    public function __construct(array $databaseConfig)
    {
        $this->databaseConfig = $databaseConfig;
    }

    /**
     * @return EntityManager|null
     * @throws \Doctrine\ORM\ORMException
     */
    public function getEm()
    {
        if (is_null($this->em)) {
            $config = Setup::createAnnotationMetadataConfiguration(
                array("/data"),
                true,
                null,
                null,
                false
            );
            $this->em = EntityManager::create($this->databaseConfig, $config);
        }

        return $this->em;
    }

}