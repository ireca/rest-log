<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20210917121637 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $table = $schema->getTable('lg_log');
        $table->addIndex(array('createdAt', 'command'), 'createdAt_2');
        $table->addIndex(array('createdAt', 'iteration'), 'createdAt_3');
        $table->addIndex(array('createdAt', 'companyId', 'sourceId'), 'createdAt_4');
        $table->addIndex(array('iteration'), 'iteration');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $table = $schema->getTable('lg_log');
        $table->addIndex(array('createdAt', 'command'), 'createdAt_2');
        $table->addIndex(array('createdAt', 'iteration'), 'createdAt_3');
        $table->addIndex(array('createdAt', 'companyId', 'sourceId'), 'createdAt_4');
        $table->addIndex(array('iteration'), 'iteration');
    }

}
