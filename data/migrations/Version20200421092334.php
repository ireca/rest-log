<?php
namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20200421092334 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->connection->executeQuery("
            CREATE TABLE `lg_log` (
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `companyId` bigint(20) DEFAULT NULL,
              `sourceId` int(11) DEFAULT NULL,
              `userId` int(11) DEFAULT NULL,
              `level` tinyint(3) unsigned NOT NULL DEFAULT '1',              
              `command` varchar(50) NOT NULL DEFAULT '',
              `commandType` varchar(20) NOT NULL DEFAULT '',
              `count` int(11) DEFAULT 0,
              `entity` varchar(100) NOT NULL DEFAULT '',
              `url` varchar(255) NOT NULL DEFAULT '',
              `title` varchar(1500) NOT NULL DEFAULT '',
              `responseCode` smallint(5) unsigned DEFAULT NULL,
              `application` varchar(10) NOT NULL DEFAULT '',
              `sessionId` varchar(32) DEFAULT '',
              `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `companyId_2` (`companyId`,`createdAt`),
              KEY `createdAt` (`createdAt`),
              KEY `companyId` (`companyId`,`sourceId`,`userId`,`createdAt`)
            ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8
        ");

        $this->connection->executeQuery("
            CREATE TABLE `lg_log_data` (
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `logId` bigint(20) unsigned NOT NULL,
              `data` mediumblob NOT NULL,
              PRIMARY KEY (`id`),
              KEY `logId` (`logId`),
              CONSTRAINT `lg_log_data_ibfk_1` FOREIGN KEY (`logId`) REFERENCES `lg_log` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");

        $this->connection->executeQuery("
            CREATE TABLE `lg_log_entity` (
              `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
              `logId` bigint(20) unsigned NOT NULL,
              `companyId` bigint(20) unsigned DEFAULT NULL,
              `sourceId` int(11) DEFAULT NULL,
              `entity` varchar(100) NOT NULL DEFAULT '',
              `code` varchar(32) DEFAULT NULL,
              `appCode` varchar(32) DEFAULT NULL,
              `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `companyId` (`companyId`,`sourceId`,`createdAt`,`entity`,`code`),
              KEY `companyId_2` (`companyId`,`createdAt`,`sourceId`,`entity`,`appCode`),
              KEY `logId` (`logId`),
              CONSTRAINT `lg_log_entity_ibfk_1` FOREIGN KEY (`logId`) REFERENCES `lg_log` (`id`) ON DELETE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->dropTable('`lg_log`');
        $schema->dropTable('`lg_log_data`');
        $schema->dropTable('`lg_log_entity`');
    }

}
