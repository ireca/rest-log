<?php
return array(
    'restLog' => array(
        'isActive' => true,
        'databaseConfig' => include (__DIR__ . '/database-config.php')
    )
);