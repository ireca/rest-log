# Rest Log - writes messages, requests and responses from some Services or Applications to the MySQL database. 

You can use this library in your project for simply and faster developed. 

## Installation

Install the latest version with

```bash
$ composer require ireca/rest-log
```

## Basic Usage

```php
<?php
$config = array(
    'restLog' => array(
        'isActive' => true,
        'databaseConfig' => array(
            'host'     => 'localhost',
            'port'     => '3306',
            'driver'   => 'pdo_mysql',
            'user'     => 'root',
            'password' => '',
            'dbname'   => 'database_name',
            'charset' => 'UTF8',
        )
    )
);

$companyId = 123;
$userId = 35;
$shopId = 18;

// Below example with use Logger for writes simple messages.
$shortMessLogService = \RestLog\Factory\MessageLogFactory::createByConfig($config);
$shortMessLogService
  ->setIteration(microtime(true))
  ->init($companyId, $userId, $shopId);

$shortMessLogService->error('I got error!');   
$shortMessLogService->critical("I got critical error!"); 
$shortMessLogService->debug("This is debug mwssage!");
$shortMessLogService->info("This is information message!");

// This example shows how use Logger for journal application request. 
$appLogservice = \RestLog\Factory\AppLogFactory::createByConfig($config);
$appLogservice
  ->setIteration(microtime(true))
  ->setApplication("Courier")
  ->init($companyId, $userId, $shopId)
 
$appRequest = '[{"id":1, "title":"This request from application"}]';
$requestIds = [1]
$sessionId = '038922f06024956b327060ad82c6668a';
$httpCode = 200;            
$appLogservice
  ->setData($appRequest, $appRequestId, $sessionId)
  ->postToLog('ORDER_PAY_COMMAND', 'Order', 'This is request to pay an order!');
$appLogservice
  ->setData($appResponse, $appRequestId, $sessionId)
  ->responseToLog('ORDER_PAY_COMMAND', $httpCode, 'Order', 'This is response of the pay an order command!');  
    
// This example shows how use Logger for journal a request from some service. 
$logService = \RestLog\Factory\RestLogFactory::createByConfig($config);
$logService
  ->setIteration(microtime(true))
  ->init($companyId, $userId, $shopId);
  
$requests = '[{"id":1, "title":"This is an order one"}, {"id":2, "title":"This is an order two"}]';
$requestIds = [1,2]
$sessionId = '2ac0f2778c2036e903ee8024ea27594e';
$httpCode = 200;  
$logService
  ->setData($requests, $requestIds, $sessionId)
  ->setCount(2)
  ->postToLog("CREATE_ORDERS", "Order", "This is create orders request");
  
try {
 // Here your logic!
} catch (\Exception $ex) {
  $logService->exceptionToLog($ex, "Oh, fuck! This is an exception!");  
}    
  
$logService
  ->setData($appRequest, $apprequestId, $sessionId)
  ->responseToLog('ORDER_PAY_COMMAND', $httpCode, 'Order', 'This is create orders response');

```

### Requirements

- Rest Log works with PHP 5.4 or above.

### Submitting bugs and feature requests

Bugs and feature request are tracked on [GitLab](https://gitlab.com/ireca/rest-log/-/issues)

### Framework Integrations

- Frameworks and libraries using [PSR-4](https://www.php-fig.org/psr/psr-4/)
- [Symfony](http://symfony.com);
- [Zend](https://framework.zend.com/);
- [Yii](https://www.yiiframework.com/) and other.

### Author

Anton Yurchenko - <antonioo83@mail.ru>

### License

Rest log is licensed under the MIT License - see the `LICENSE` file for details